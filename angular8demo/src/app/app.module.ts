import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule, MatDatepickerModule, MatCheckboxModule, MatButtonModule, MatFormFieldModule, MatSelectModule } from '@angular/material';
import { MatMomentDateModule } from "@angular/material-moment-adapter";
import {MatMenuModule} from '@angular/material/menu';
import {MatToolbarModule} from '@angular/material/toolbar';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { MatSidenavModule } from '@angular/material/sidenav'


import { IndexAddressComponent } from './components/Address/index/index.component';
import { CreateAddressComponent } from './components/Address/create/create.component';
import { EditAddressComponent } from './components/Address/edit/edit.component';
import { IndexCompanyComponent } from './components/Company/index/index.component';
import { CreateCompanyComponent } from './components/Company/create/create.component';
import { EditCompanyComponent } from './components/Company/edit/edit.component';
import { IndexDepartmentComponent } from './components/Department/index/index.component';
import { CreateDepartmentComponent } from './components/Department/create/create.component';
import { EditDepartmentComponent } from './components/Department/edit/edit.component';
import { IndexDivisionComponent } from './components/Division/index/index.component';
import { CreateDivisionComponent } from './components/Division/create/create.component';
import { EditDivisionComponent } from './components/Division/edit/edit.component';
import { IndexEmployeeComponent } from './components/Employee/index/index.component';
import { CreateEmployeeComponent } from './components/Employee/create/create.component';
import { EditEmployeeComponent } from './components/Employee/edit/edit.component';

import * as appRoutes from './routerConfig';

import { AddressService } from './services/Address.service';
import { CompanyService } from './services/Company.service';
import { DepartmentService } from './services/Department.service';
import { DivisionService } from './services/Division.service';
import { EmployeeService } from './services/Employee.service';

@NgModule({
  declarations: [
    IndexAddressComponent,
    CreateAddressComponent,
    EditAddressComponent,
    IndexCompanyComponent,
    CreateCompanyComponent,
    EditCompanyComponent,
    IndexDepartmentComponent,
    CreateDepartmentComponent,
    EditDepartmentComponent,
    IndexDivisionComponent,
    CreateDivisionComponent,
    EditDivisionComponent,
    IndexEmployeeComponent,
    CreateEmployeeComponent,
    EditEmployeeComponent,
    AppComponent
  ],
  imports: [

    BrowserModule, 
    NgbModule,
    MatMenuModule,
    MatToolbarModule,
    MatCheckboxModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
	MatMomentDateModule,
    BrowserAnimationsModule,
	HttpClientModule, 
    ReactiveFormsModule,
    FormsModule,
    MatSidenavModule,    
    RouterModule.forRoot(appRoutes.AddressRoutes), 
    RouterModule.forRoot(appRoutes.CompanyRoutes), 
    RouterModule.forRoot(appRoutes.DepartmentRoutes), 
    RouterModule.forRoot(appRoutes.DivisionRoutes), 
    RouterModule.forRoot(appRoutes.EmployeeRoutes), 
  ],
  providers: [AddressService,CompanyService,DepartmentService,DivisionService,EmployeeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
