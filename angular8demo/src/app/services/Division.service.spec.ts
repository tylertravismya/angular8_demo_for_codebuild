import { TestBed } from '@angular/core/testing';
import { FormGroup,  FormBuilder,  Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

import { DivisionService } from './Division.service';

describe('DivisionService', () => {
  	beforeEach(() => {
	  TestBed.configureTestingModule({ imports: [HttpClient, FormGroup, FormBuilder, Validators], providers: [DivisionService] });
	});

  it('should be created', () => {
    const service: DivisionService = TestBed.get(DivisionService);
    expect(service).toBeTruthy();
  });
});
