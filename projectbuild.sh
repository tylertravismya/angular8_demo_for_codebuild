#!/bin/bash
mkdir appRoot
cd appRoot && ng new angular8demo --defaults
cp -r -n /appRoot/angular8demo/ /gitRoot/
cd /gitRoot/angular8demo && npm install --prod && npm run setup
cd /gitRoot/angular8demo && ng build
cp -r -n /gitRoot/ /code/    
cp -r -n /gitRoot/ /code/